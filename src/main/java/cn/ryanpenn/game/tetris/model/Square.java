package cn.ryanpenn.game.tetris.model;

/**
 * Square
 *
 * @author pennryan
 */
public class Square {

    int type;
    int index;

    public Square() {
    }

    public Square(int type, int index) {
        this.type = type;
        this.index = index;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

}

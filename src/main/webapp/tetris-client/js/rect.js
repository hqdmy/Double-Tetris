define(['jquery'], function ($) {

    // 定义矩形类
    let Rectangle = function (parent, padding) {

        let x = 0, y = 0, w = 0, h = 0, pad;
        if (parent) {
            x = parent.x;
            y = parent.y;
            w = parent.width;
            h = parent.height;
        }

        if (padding) {
            pad = padding;
            x += padding.left;
            y += padding.top;
            w -= (padding.left + padding.right);
            h -= (padding.top + padding.bottom);
        } else {
            pad = Padding.Empty;
        }

        return {
            x: x,
            y: y,
            width: w,
            height: h,
            padding: pad,
            pos: {
                x: x,
                y: y
            }
        }
    };

    let Padding = function (left, top, right, bottom) {
        return {
            left: left,
            top: top,
            right: right,
            bottom: bottom
        }
    };
    Padding.all = function (all) {
        return new Padding(all, all, all, all);
    };
    Padding.Empty = {
        left: 0,
        top: 0,
        right: 0,
        bottom: 0
    };

    let Window = new Rectangle({
        x: 0,
        y: 0,
        width: $(window).width(),
        height: $(window).height()
    }, Padding.Empty);

    let AutoFitRect = function (parent, ratio) {
        // 界面默认比例为 1:1
        let _ratio = {
            x: 1,
            y: 1
        };
        if (ratio && ratio.x && ratio.y) {
            _ratio.x = ratio.x;
            _ratio.y = ratio.y;
        }

        // 根据宽,高比自适应界面
        let workWidth = parent.width,
            workHeight = parent.height;
        if (workWidth > workHeight * _ratio.x / _ratio.y) {
            workWidth = workHeight * _ratio.x / _ratio.y;
        } else if (workHeight > workWidth * _ratio.y / _ratio.x) {
            workHeight = workWidth * _ratio.y / _ratio.x;
        }

        // 在父容器中居中
        let workBeginX = (parent.width - workWidth) / 2,
            workBeginY = (parent.height - workHeight) / 2;

        return new Rectangle({
            x: (parent.width - workWidth) / 2,
            y: (parent.height - workHeight) / 2,
            width: workWidth,
            height: workHeight
        }, Padding.Empty);
    };

    // 导出API
    return {
        Rectangle: Rectangle,
        AutoFitRect: AutoFitRect,
        Padding: Padding,
        WindowRect: Window
    }
});
package cn.ryanpenn.game.tetris.game;

import com.corundumstudio.socketio.AuthorizationListener;
import com.corundumstudio.socketio.HandshakeData;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * LoginAuthorization
 *
 * @author pennryan
 */
public class LoginAuthorization implements AuthorizationListener {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 登录验证
     * io.connect('ws://localhost:8081?name=test&pass=test');
     *
     * @param handshakeData 握手数据
     * @return
     */
    @Override
    public boolean isAuthorized(HandshakeData handshakeData) {
        String username = handshakeData.getSingleUrlParam("name");
        String password = handshakeData.getSingleUrlParam("pass");

        logger.info("username: {}, password: {}.", username, password);

        if (!StringUtils.isEmpty(username) && !StringUtils.isEmpty(password)) {
            // TODO: 仅用于测试,只要用户名和密码相同就可以登录成功
            return StringUtils.equalsAnyIgnoreCase(username, password);
        }
        return false;
    }
}

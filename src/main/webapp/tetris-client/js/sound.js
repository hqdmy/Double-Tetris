define([], function () {

    // 音效来源  http://www.aigei.com/
    var SoundPlayer = function () {
        let sounds = [];

        let addSrc = function (id, src, loop) {
            let audio = new Audio(src);
            audio.id = id;
            audio.loop = loop;
            sounds.push(audio);
            return audio;
        };

        let playAll = function () {
            sounds.forEach(audio => {
                audio.play();
            });
        };

        let get = function (id) {
            sounds.forEach(audio => {
                if (id == audio.id) {
                    return audio;
                }
            });
        };

        return {
            add: addSrc,
            playAll: playAll,
            get: get
        }
    };

    // 导出
    return {
        SoundPlayer: SoundPlayer
    }
});

/* 用法:
 let player = new SoundPlayer();
 let bg = player.add('bgm', 'sound/bgmusic.mp3', true);
 let sound = player.add('click', 'sound/click.mp3', false);
 player.playAll(); // 可同时播放多个音频

 $("html").bind('click', function () {
     sound.play();

     // 音频可单独控制
     if (bg.paused) {
        bg.play();
     } else {
        bg.pause();
     }
 });
 */
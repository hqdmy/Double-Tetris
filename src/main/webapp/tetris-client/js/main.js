require.config({
    baseUrl: 'js/',
    paths: {
        "jquery": ["https://cdn.bootcss.com/jquery/3.3.1/jquery.min"],
        "pixi": ["https://cdn.bootcss.com/pixi.js/4.7.0/pixi.min"],
        "io": ["https://cdn.bootcss.com/socket.io/1.7.4/socket.io.min"]
    }
});

require(['jquery', 'pixi', 'tetris'], function ($, PIXI, tetris) {
    var type = "WebGL";
    if (!PIXI.utils.isWebGLSupported()) {
        type = "canvas";
    }
    PIXI.utils.sayHello(type);

    tetris.start('http://localhost:8080/?name=aaa&pass=aaa');
});
package cn.ryanpenn.game.tetris.model;

import com.corundumstudio.socketio.SocketIOClient;

/**
 * Client
 *
 * @author pennryan
 */
public class Client {

    String uuid;
    SocketIOClient io;
    Square data;

    public Client() {
    }

    public Client(String uuid, SocketIOClient io) {
        this.uuid = uuid;
        this.io = io;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public SocketIOClient getIo() {
        return io;
    }

    public void setIo(SocketIOClient io) {
        this.io = io;
    }

    public Square getData() {
        return data;
    }

    public void setData(Square data) {
        this.data = data;
    }
}
